## A little experiment viewing GitHub data

This is my fist application using React + Redux + React Router + TypeScript.
It was fun to put it together even if, I'm sure, there are some "first time"-ingenuities. I'm missing some types for dispatch and I decided to put a shameful "any" to unblock myself.

I decided to use the octokit API to retrieve data from GitHub.

Right now the app is tested only on Chrome and it will work in "debug" mode: therefore you will see the redux-devtools Dock. I decided to leave it.

I had just a little bit of fun with data visualization and added a tab to show the JSON data about a user in a nice way.

I spent some time making browser navigation work: if you use the Next button in the UI to load the next page of data, then you can use the browser Back button to go back. Also if you click on user details you can, then, click the browser Back button to go back.
Clicking on the "logo" in the NavBar you will go back to the first page. All this means that the details view and the list view are "bookmarkable": you can bookmark a user details page and also a specific position in the user list. I think that this is a good UX "trick" that can help users (even if, for this example, it might be overkill).

There is no error handling, while I defined the error actions, I did not implement anything to show errors, shame on me.

I admit that code is not really commented up to production standards.

The app has been generated with "create-react-app --typescript". In order to launch it, please do an "npm i" and then "npm run start".