import { Store, createStore, applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { createLogger} from 'redux-logger';
import appReducer from '../reducers';
import DevTools from '../containers/DevTools';
import {IUserViewerState} from '../types/AppState';


const configureStore = (initialState?: IUserViewerState): Store<IUserViewerState> => {
    const store = createStore(
        appReducer,
        initialState,
        compose(
            applyMiddleware(
                thunkMiddleware,  
                createLogger()
            ),
            DevTools.instrument()
        )
    );

    // Shamelessly copied from redux tutorial.
    if ( (module as any).hot) {
        ( module as any).hot.accept('../reducers', () => {
            store.replaceReducer(appReducer);
        })
    }

    return store;
}

export default configureStore;