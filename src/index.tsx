import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';

import * as serviceWorker from './serviceWorker';

import App from './containers/App';

import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';

import configureStore from './store/configureStore';

const store = configureStore();

console.log(store.getState());

import {loadUsersSince} from './actions';


// NOTE: <any> should not be there, but I did not find a better solution.
//store.dispatch<any>(loadUsersSince());//.then( () => console.log(store.getState()));

ReactDOM.render(
    <Router>
        <App store={store}/>
    </Router>, 
    document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
