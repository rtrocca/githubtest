import { ThunkAction } from  'redux-thunk';
import { Action, ActionCreator, Dispatch} from 'redux';
import { IUserViewerState } from '../types/AppState';
import octokit from '@octokit/rest';

export enum UsersListActionType {
    LOAD_USERS_REQUEST = 'LOAD_USERS_REQUEST',
    LOAD_USERS_FAILURE = 'LOAD_USERS_FAILURE',
    LOAD_USERS_SUCCESS = 'LOAD_USERS_SUCCESS',
    LOAD_USER_DATA_REQUEST = 'LOAD_USER_DATA_REQUEST',
    LOAD_USER_DATA_FAILURE = 'LOAD_USER_DATA_FAILURE',
    LOAD_USER_DATA_SUCCESS = 'LOAD_USER_DATA_SUCCESS',
}

interface ILoadUsersRequestAction extends Action {
    type: UsersListActionType.LOAD_USERS_REQUEST;
    sinceId? : string;
}

interface ILoadUsersFailureAction extends Action {
    type: UsersListActionType.LOAD_USERS_FAILURE;
}

interface ILoadUsersSuccessAction extends Action {
    type: UsersListActionType.LOAD_USERS_SUCCESS,
    sinceId? : string;
    users: octokit.UsersListResponseItem[];
}

export type ILoadUsersAction = ILoadUsersRequestAction & ILoadUsersFailureAction & ILoadUsersSuccessAction;

const octo = new octokit();

export const loadUsersRequest: ActionCreator<ILoadUsersRequestAction>  = ( sinceId?: string) => ({
        type: UsersListActionType.LOAD_USERS_REQUEST,
        sinceId: sinceId
});

export const loadUsersFailure: ActionCreator<ILoadUsersFailureAction> = ( sinceId?: string) => ({
    type: UsersListActionType.LOAD_USERS_FAILURE, 
    sinceId
});

export const loadUsersSuccess: ActionCreator<ILoadUsersSuccessAction> = ( users: octokit.UsersListResponseItem[], sinceId?: string) => ({
        type: UsersListActionType.LOAD_USERS_SUCCESS, 
        sinceId,
        users
});

export const loadUsersSince: ActionCreator<
                            ThunkAction<
                                void, 
                                IUserViewerState , 
                                undefined, 
                                ILoadUsersRequestAction>
                            > = (sinceId?: string) => {

    return (dispatch: Dispatch<ILoadUsersRequestAction | ILoadUsersSuccessAction>): void => {
        dispatch(loadUsersRequest(sinceId));
        octo.users.list({since: sinceId}).then(
            result => {
                dispatch( loadUsersSuccess(result.data, sinceId));
            }
        );

    }
};

interface ILoadUserDataRequestAction extends Action {
    type: UsersListActionType.LOAD_USER_DATA_REQUEST;
    userAlias : string;
}

interface ILoadUserDataFailureAction extends Action {
    type: UsersListActionType.LOAD_USER_DATA_FAILURE;
    userAlias : string;
}

interface ILoadUserDataSuccessAction extends Action {
    type: UsersListActionType.LOAD_USER_DATA_SUCCESS;
    userAlias : string;
    userData: octokit.UsersListResponseItem;
}

export type ILoadUserDataAction = ILoadUserDataRequestAction & ILoadUserDataFailureAction & ILoadUserDataSuccessAction;

export const loadUserDataRequest: ActionCreator<ILoadUserDataRequestAction> = (userAlias: string) => {
    return {type: UsersListActionType.LOAD_USER_DATA_REQUEST, userAlias};
}
export const loadUserDataFailure: ActionCreator<ILoadUserDataFailureAction> = (userAlias: string) => {
    return {type: UsersListActionType.LOAD_USER_DATA_FAILURE, userAlias};
}
export const loadUserDataSuccess: ActionCreator<ILoadUserDataSuccessAction> = (userAlias: string, userData: octokit.UsersListResponseItem) => {
    return {type: UsersListActionType.LOAD_USER_DATA_SUCCESS, userAlias, userData};
}

export const loadUserData: ActionCreator<
                            ThunkAction<
                                void, 
                                IUserViewerState , 
                                undefined, 
                                ILoadUserDataRequestAction>
                            > = (userAlias: string) => {

    return (dispatch: Dispatch<ILoadUserDataRequestAction | ILoadUserDataSuccessAction>): void => {
        dispatch(loadUserDataRequest(userAlias));

        octo.users.getByUsername({username: userAlias}).then(
            result => {
                dispatch( loadUserDataSuccess(userAlias, result.data));
            }
        );
    }
};