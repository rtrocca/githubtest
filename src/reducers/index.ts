import { combineReducers, Reducer} from 'redux';
import { IUserViewerState, IUserListState, ICurrentUserState} from '../types/AppState';

import {
    UsersListActionType,
    ILoadUsersAction,
    ILoadUserDataAction
}  from '../actions';

const initialState: IUserViewerState = {
    usersList: {
        isLoading: false,
        users: [],
        sinceId: null
    },
    currentUser: {
        alias: null,
        isLoading: false,
        data: null
    }
};

export function usersList(state: IUserListState = initialState.usersList, action: ILoadUsersAction) {
    switch (action.type) {
        case UsersListActionType.LOAD_USERS_REQUEST:
            return Object.assign(
                {},
                state,
                {
                    isLoading: true, 
                    users: []
                }
            );
        case UsersListActionType.LOAD_USERS_FAILURE:
            return Object.assign(
                {},
                state,
                {
                    isLoading: false, 
                    users: []
                }
            );
            case UsersListActionType.LOAD_USERS_SUCCESS:
            return Object.assign(
                {},
                state,
                {
                    isLoading: false, 
                    users: action.users,
                    nextUserId: action.users[action.users.length - 1].id
                }
            );
        default:
            return state;
    }
}

export function currentUser(state: ICurrentUserState = initialState.currentUser, action: ILoadUserDataAction) {
    switch (action.type) {
        case UsersListActionType.LOAD_USER_DATA_REQUEST:
            return Object.assign(
                {}, 
                state, 
                { 
                    alias: action.userAlias, 
                    isLoading: true, 
                    data: null
                }
            );
        case UsersListActionType.LOAD_USER_DATA_SUCCESS:
                return Object.assign(
                    {},
                    state,
                    {
                        alias: action.userAlias,
                        data: action.userData,
                        isLoading: false
                    }
                );
        case UsersListActionType.LOAD_USER_DATA_FAILURE:
                return Object.assign(
                    {},
                    state,
                    {
                        alias: action.userAlias,
                        data: null,
                        isLoading: false
                    }
                );
        default:
            return state;

    }
}

const userApp:Reducer<IUserViewerState> = combineReducers({
    usersList,
    currentUser
});

export default userApp;
