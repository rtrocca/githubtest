// NOTE: it would be better not to pollute the state with types that are dependant on external APIs, but
// for the sake of time and because this is only an exercise, I'll be lazy :-)
import octokit from '@octokit/rest';

export interface IUserListState {
        isLoading: boolean;
        users: octokit.UsersListResponseItem[];
        sinceId: string | null;
}

export interface ICurrentUserState {
    alias: string | null,
    isLoading: boolean,
    data: null
}

export interface IUserViewerState {
        usersList: IUserListState;
        currentUser: ICurrentUserState
}