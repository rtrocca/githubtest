// Wrap the List component so that it has access to redux.

import {connect} from 'react-redux';
import { 
    loadUsersSince
} from '../actions';

import List, {IListProps, IListMethodsProps} from '../components/List';
import { IUserViewerState} from '../types/AppState';

const mapStateToProps = (state: IUserViewerState, ownProps: any): IListProps => {
    console.log('ownProps', ownProps);
    return {
        users: state.usersList.users,
        isLoading: state.usersList.isLoading,
        currentSinceId: state.usersList.sinceId,
        sinceId: ownProps.match.params.sinceId
    };
}
// TODO any is of course the wrong type
const mapDispatchToProps = (dispatch: any): IListMethodsProps  => ({
    loadData: (lastId?: string) => { 
        return dispatch(loadUsersSince(lastId));
    }
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(List);