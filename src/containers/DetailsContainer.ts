import { connect } from 'react-redux';
import { loadUserData } from '../actions'
import { IUserViewerState} from '../types/AppState';
import Details, {IDetailsProps, IDetailsMethodsProps} from '../components/Details';


const mapStateToProps = (state: IUserViewerState, ownProps: any): IDetailsProps => {
    console.log('ownProps', ownProps);
    return {
        user: state.currentUser.data,
        userAlias: ownProps.match.params.username,//state.currentUser.alias,
        isLoading: state.currentUser.isLoading
    };
};

// NOTE: dispatch should not be of type any
const mapDispatchToProps = (dispatch: any): IDetailsMethodsProps  => ({
    loadUser: (userAlias: string) => { 
        return dispatch(loadUserData(userAlias));
    }
})


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Details);