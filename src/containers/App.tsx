/*
if (process.env.NODE_ENV === 'production') {
  module.exports = require('./Root.prod')
} else {
  module.exports = require('./Root.dev')
}
*/


import React, { Component } from 'react';
import { Provider } from 'react-redux';
import {  Route } from 'react-router-dom';
import './App.css';

import Header from '../components/Header';
import Details from './DetailsContainer';

import UserList from './UserListContainer';

import DevTools from './DevTools';

interface IAppProps {
  store: any
}

class App extends Component<IAppProps>{
  render() {
    return (
      <Provider store={this.props.store}>
        <div>
          <Header></Header>
          <div className="container-fluid" style={{marginTop: '50px'}}>
              <Route exact path="/:sinceId?" component={UserList}></Route>
              <Route exact path="/details/:username" component={Details}></Route>
              <DevTools/>
          </div>
        </div>
        </Provider>
    );
  }
}

export default App;
