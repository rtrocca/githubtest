import React, {Component} from 'react';

import {
    Navbar,
    NavbarBrand,
} from 'reactstrap';

class Header extends Component {
    render() {
        return (

                <Navbar color="dark" dark expand="md">
                    <NavbarBrand href="/">GitHub User Viewer</NavbarBrand>
                </Navbar>
        );
    }
}

export default Header;