import React, {Component} from 'react';
import {UsersGetByUsernameResponse} from '@octokit/rest';
import ReactJson from 'react-json-view';
import {Nav, NavItem, TabContent, TabPane} from 'reactstrap';
import NavLink from 'reactstrap/lib/NavLink';

import './Details.css';

export interface IDetailsProps {
    user: UsersGetByUsernameResponse | null;
    userAlias: string | null;
    isLoading: boolean;
}

export interface IDetailsMethodsProps {
    loadUser(userAlias: string): void;
}

interface IDetailsState {
    activeTab: string;
}

class Details extends Component<IDetailsProps & IDetailsMethodsProps, IDetailsState> {

    constructor(props: IDetailsProps & IDetailsMethodsProps) {
        super(props);
        this.state = {
            activeTab: '1'
        };
    }

    componentDidMount() {
        if (this.props.userAlias) {
            this.props.loadUser(this.props.userAlias);
        }
    }

    renderJson() {
        if (this.props.user) {
            return (<ReactJson src={this.props.user}></ReactJson>);
        }
        else {
            return (<div> No Data</div>);
        }
    }
    toggle(tab: string): void {
        this.setState({...this.state, activeTab: tab});
    }

    renderLoading(): JSX.Element {
        return( <div> Loading ... </div>)
    }

    renderInfoRow(label: string, value: string): JSX.Element {
        return (
            <div className="row info-row">
                <div className="col-md-6">
                    <strong>{label}</strong>
                </div>
                <div className="col-md-6">
                    <span>{value}</span>
                </div>
            </div>
        );
    }

    renderInfoRowLink(label: string, value: string, url: string): JSX.Element {
        return (
            <div className="row info-row">
                <div className="col-md-6">
                    <strong>{label}</strong>
                </div>
                <div className="col-md-6">
                    <a href={url} target="_blank">{value}</a>
                </div>
            </div>
        );
    }

    renderData(user: UsersGetByUsernameResponse ): JSX.Element {
        
        return (
            <div>

                <Nav tabs>
                    <NavItem>
                        <NavLink
                            className={this.state.activeTab === '1'? 'active': ''}
                            onClick={ () => {this.toggle('1');} }
                        >
                           Human friendly details
                        </NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink
                            className={this.state.activeTab === '2'? 'active': ''}
                            onClick={ () => {this.toggle('2');} }
                        >
                             Geeky Details
                            
                        </NavLink>
                    </NavItem>
                </Nav>
                <TabContent activeTab={this.state.activeTab}>
                    <TabPane tabId="1">
                        <div className="tab-content">
                            <div className="row">
                                <div style={{ display: 'inline-block'}}>
                                    <img src={this.props.user!.avatar_url} width='200px'></img>
                                </div>
                                <div style={{ display: 'inline-block', width: '40%', marginLeft: '30px'}}>
                                    {this.renderInfoRow('Name', user.name)}

                                    { user.company ? this.renderInfoRow('Company', user.company) : ''}
                                    { user.location ? this.renderInfoRow('Location', user.location): ''} 
                                    { user.blog ? this.renderInfoRowLink('Blog', user.blog, user.blog): ''}
                                    { this.renderInfoRow('Followers', `${user.followers}`)}
                                    { this.renderInfoRow('Following', `${user.following}`)}
                                    { this.renderInfoRow('Public Repositories', `${user.public_repos}`)}
                                </div>
                            </div>
                            
                        </div>

                    </TabPane>
                    <TabPane tabId="2">
                        <div className="tab-content">
                            {this.renderJson()}
                        </div>
                    </TabPane>
                </TabContent>

            </div>
        );
    }

    render(): JSX.Element {

        return (<div>
            <h4>Details {this.props.userAlias }</h4>
            {this.props.isLoading ? 
                this.renderLoading() : 
                
                this.props.user ? this.renderData(this.props.user) : this.renderLoading()
            }
        </div>)
    }
}

export default Details;