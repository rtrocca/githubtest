// UserCard only makes the app navigate to the Details view, setting a URL parameter. 
// The Details component will use the parameters from the URL to load the data. In this way the details URL is "bookmarkable".
import React, {Component} from 'react';
import {withRouter} from "react-router";
import {History} from 'history';
import {UsersListResponseItem} from '@octokit/rest';
import './UserCard.css';

interface ICardProps {
    user: UsersListResponseItem;
    match: any;
    location: any;
    history: History;
}

class UserCard extends Component<ICardProps> {

    goToDetails() {
        this.props.history.push(`/details/${this.props.user.login}`);
    }

    render() {
        return (
            <div className='gh-card'>
                <div>
                    <img src={this.props.user.avatar_url}
                         width='140px'
                         className='pointer'
                         onClick={()=> this.goToDetails()}></img>
                </div>

                <div className='gh-card-data'>
                    <a className='name pointer' title={`${this.props.user.id}`}>{this.props.user.login}</a><br/>
                    <a className='more pointer float-right' href={this.props.user.html_url} rel="external" target="_blank">More&hellip;</a>
                </div>
            </div>
        );
    }
}

const UserCardWithRouter = withRouter(UserCard);
export default UserCardWithRouter;