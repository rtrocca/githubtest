import React, {Component} from 'react';
import octokit from '@octokit/rest';
import {History, UnregisterCallback} from 'history';
import UserCard from './UserCard';//'../containers/UserCardContainer';
import './List.css';

export interface IListProps {
    users: octokit.UsersListResponseItem[];
    isLoading: boolean;
    currentSinceId: string | null; //sinceId used to build the store
    sinceId: string; //sinceId in the URL
}

export interface IListMethodsProps {
    loadData: (sinceId?: string) => void;
}


class List extends Component<IListProps & IListMethodsProps & { history: History}> {

    private unregister?: UnregisterCallback;

    componentDidMount() {
        if (!this.props.isLoading) {
            this.props.loadData(this.props.sinceId);   
        }

        this.unregister = this.props.history.listen( (location) => {
            // The location changed, but props still did not change.
            // Let's extract the sinceId parameter from the URL. There must be a better way than this.
            let sinceId = location.pathname.length > 1 ? location.pathname.substring(1) : '';
            if (sinceId !== this.props.currentSinceId && !this.props.isLoading) {
                this.props.loadData(sinceId);
            }
        });
    }

    componentWillUnmount() {
        this.unregister? this.unregister() : null;
    }

    renderLoading():JSX.Element {
        return <div>
            Loading ...
        </div>
    }

    renderList(): JSX.Element {
        return (
            <div>
                <div className='gh-list'>
                    {this.props.users? this.props.users.map( (user) => {
                        return <UserCard key={user.id} user={user}></UserCard>
                    }): ''}
                </div>
            <button className="btn btn-light float-left" onClick={
                () => { 
                    this.next();
                }
            }> Next</button>
        </div>
        
        );
    }
    render(): JSX.Element {
        if (this.props.isLoading) {
            return this.renderLoading();
        } else {
            return this.renderList();
        }
    }

    next() {
        
        if (this.props.users && this.props.users.length) {
            let id = this.props.users[this.props.users.length - 1].id;
            this.props.history.push(`/${id}`);
        }
    }
}

export default List;